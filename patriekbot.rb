require "teams_webhook" # gem for posting messages to Microsoft Teams
require "rss" # standard library for parsing RSS feeds

# Set the webhook URL for the Microsoft Teams channel
webhook_url = "https://outlook.office.com/webhook/..."

# Set the URL of the RSS feed
rss_url = "https://www.nu.nl/rss/Achterklap"

# Parse the RSS feed
feed = RSS::Parser.parse(rss_url)

# Iterate over the items in the feed
feed.items.each do |item|
  # Check if the item's title matches "Hazes"
  if item.title.include?("Hazes")
    # Post the item's title and link to Microsoft Teams
    TeamsWebhook.send(webhook_url, {
      "title": item.title,
      "link": item.link
    })
  end
end
